# my-node-app
This is a complete CI\CD for an application in node. It includes:
- build a docker image and update the version at each build
- push the image to a Runner
- run tests in multi-stages
- deploy to multi-servers
- deploy to prod with manual approve


## Setup
- at least 1 hosted runner
- at least 1 vm reachable by the runner (I use vagrant)

